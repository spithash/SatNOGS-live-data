# SatNOGS Live Data

SatNOGS Status and live information via API call written in python

## Installation
Just rename or copy [configuration.ini-dist](https://gitlab.com/spithash/SatNOGS-live-data/-/blob/master/configuration.ini-dist) to configuration.ini and replace 'YOUR_API_KEY_GOES_HERE' with your API key (without the '')

### Python Requirements
- pandas
- pandas_streaming
- configparser
- sklearn
- time
- sys
- re
- signal
